FROM ribadev/ev:0.0.1
MAINTAINER ti@estantevirtual.com.br

RUN mkdir -p /srv/menus
RUN mkdir -p /srv/shared/pids
RUN mkdir -p /srv/shared/sockets
RUN mkdir -p /srv/shared/log

WORKDIR /srv/menus
ADD Gemfile /srv/menus/Gemfile
ADD Gemfile.lock /srv/menus/Gemfile.lock

RUN bundle install
ADD . /srv/menus
