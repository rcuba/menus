class CreateMenuSettings < ActiveRecord::Migration
  def change
    create_table :menu_settings do |t|
      t.belongs_to :feature, index: true, foreign_key: true
      t.belongs_to :menu, index: true, foreign_key: true
      t.integer :parent_id

      t.timestamps null: false
    end

    add_index :menu_settings, :parent_id
  end
end
