class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.string :href
      t.string :name
      t.string :title
      t.string :description

      t.timestamps null: false
    end
  end
end
