class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.text :description
      t.string :label
      t.timestamps null: false
    end

    add_index :menus, :label, unique: true
  end
end
