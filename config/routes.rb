Rails.application.routes.draw do
  resources :menus, defaults: { format: :json }
  resources :menu_settings, defaults: { format: :json }
  resources :features, defaults: { format: :json }
end
