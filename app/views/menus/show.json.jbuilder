json.extract! @menu, :id, :description, :label, :created_at, :updated_at
json.children do
  json.array! @menu.children, 'menus/children', as: :menu
end
