json.array!(@menus) do |menu|
  json.extract! menu, :id, :description, :label
  json.url menu_url(menu, format: :json)
end
