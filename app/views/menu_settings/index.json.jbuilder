json.array!(@menu_settings) do |menu_setting|
  json.extract! menu_setting, :id, :feature_id, :menu_id
  json.url menu_setting_url(menu_setting, format: :json)
end
