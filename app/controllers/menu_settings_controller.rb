class MenuSettingsController < ApplicationController
  before_action :set_menu_setting, only: [:show, :edit, :update, :destroy]

  def index
    @menu_settings = MenuSetting.all
  end

  def show
  end

  def new
    @menu_setting = MenuSetting.new
  end

  def edit
  end

  def create
    @menu_setting = MenuSetting.new(menu_setting_params)

    if @menu_setting.save
      render :show, status: :created, location: @menu_setting
    else
      render json: @menu_setting.errors, status: :unprocessable_entity
    end
  end

  def update
    if @menu_setting.update(menu_setting_params)
      render :show, status: :ok, location: @menu_setting
    else
      render json: @menu_setting.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @menu_setting.destroy
    head :no_content
  end

  private

  def set_menu_setting
    @menu_setting = MenuSetting.find(params[:id])
  end

  def menu_setting_params
    params.require(:menu_setting).permit(:feature_id, :menu_id)
  end
end
