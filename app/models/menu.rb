class Menu < ApplicationRecord
  has_many(
    :children, -> { where(parent_id: nil) },
    class_name: MenuSetting.to_s, foreign_key: :menu_id
  )
end
