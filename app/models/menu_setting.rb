class MenuSetting < ApplicationRecord
  belongs_to :feature
  belongs_to :menu

  belongs_to :parent, class_name: MenuSetting.to_s, foreign_key: :parent_id
  has_many :children, class_name: MenuSetting.to_s, foreign_key: :parent_id
end
